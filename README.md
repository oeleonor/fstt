# FSTT

`fstt` is a **f**ree**s**tyle **t**yping **t**est which measures your WPM typed into an empty prompt.
fstt expects the user to attempt an actual typing test rather than input a series of small words. Most accurate results are provided when inputting a realistic and clear series of thoughts into the prompt. Currently, only gross WPM calculation is planned, but net WPM and accuracy results may be included in the future with an optional dictionary dependency or user validated error checking. Potential character diff when working on existing files is also planned.

### Dependencies

- `bash`
- text editor defined as `$VISUAL` or `$EDITOR`, or `vi`

### Usage

```sh
Usage: fstt {OPTION}
Example: fstt -w -m c

	Options:
		-m, --method		Specify a method for calculation. Default is Gross WPM.
					All methods:
						c, CPM
						g, Gross WPM
		-t, --time		Specify a time in seconds the prompt is reading input.
		-w, --write		Dump prompt input and results to stdout upon exiting.
		-h, --help		Display this message.

	Environment variables:
		FSTT_DEBUG		If set to 'true' or '1', enter debug mode.

```
